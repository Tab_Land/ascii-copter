/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.Body;
import java.util.Comparator;

/**
 *
 * @author ijtaba
 * This class was made to compare two pieces of munition.
 * The importance is based on their distance to my player, ie the heli
 * The closer a piece of ammo is, the more important
 * useful in counter attacking
 * This class is used to sort the enemy ammo list in the attackManager
 */
public class AmmunitionComparator implements Comparator{
    private Helicopter myHeli;
    
    /**
     * Reference to thine player
     * @param heliref
     */
    public AmmunitionComparator(Helicopter heliref){
        myHeli = heliref;
    }
    /**
     * 
     * @param b First body
     * @param b1 second body
     * checks to see which body is closer to heli
     * See comparator reference
     * @return
     */
    @Override
    public int compare(Object b, Object b1) {
        //get positions of bodies
        BVec2 ammo1Pos = new BVec2(((Body)b).getPosition());
        BVec2 ammo2Pos = new BVec2(((Body)b1).getPosition());
        
        //do vector subtraction to find vector to heli,
        //and then get length of said vector
        double ammo1Dist = myHeli.getPosition().sub(ammo1Pos).length();
        double ammo2Dist = myHeli.getPosition().sub(ammo2Pos).length();
        
        //compare the two distances and move on
        if(ammo1Dist>ammo2Dist) return 1;
        else return -1;
    }

    
    
}
