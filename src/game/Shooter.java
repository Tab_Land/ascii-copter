/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.Body;
import city.soi.platform.Shape;
import javax.swing.Timer;

/**
 *Shooter is a parent class for any character which can shoot e.g helicopters and tanks
 * helps share methods
 * made to be extended, hence abstract
 * @author ijtaba
 */
public abstract class Shooter extends Body{
    private int shotLimit, shotsFired;
    /**
     * make a new shooter
     * max shots are controlled from this class
     * help control number of bodies made dynamically in the game
     * @param game
     * @param maxShots
     */
    public Shooter(Game game, int maxShots){
        super(game.getWorld());
        shotLimit = maxShots;
    }
    /**
     * increment shots
     */
    public void incrementShots(){
        shotsFired++;
    };
    /**
     * decrement shots
     */
    public void decrementShots(){
        shotsFired--;
    };
    /**
     * get max shots
     * @return
     */
    public int getMaxShots(){
        return shotLimit;
    }
    /**
     * set max shots which can be fired by the player
     * @param shots
     */
    public void setMaxShots(int shots){
        shotLimit = shots;
    }
    /**
     * get the number of shots which have been fired by this shooter
     * @return
     */
    public int getShots(){
        return shotsFired;
    }
    /**
     * shoot!, and increment current number of shots fired
     * @param choice
     */
    public void shoot(String choice){
        //shoot only if enough bullets are available
        if(shotsFired<shotLimit){
            incrementShots();
            shootAmmo(choice);
        }
    }
    /*actual child of ammunition gets shot here
     * a method which helps shooters customise what the shoot
     * e.g a tank shoots canonballs
     * and a helicopter shoots missiles and bombs
    */
    abstract void shootAmmo(String choice);
}
