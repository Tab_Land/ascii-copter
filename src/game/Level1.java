/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.*;


/**
 *
 * @author ijtaba
 * First level, Aim of the game, collect all 9 collectibles
 */
public class Level1 extends Level{
    private Helicopter myHeli;
    //constructor initialises level
    private World world;
    private WorldView view;
    
    /**
     * create level
     * @param game
     */
    public Level1(Game game){
       //make new world
       world = new World(); 
       //send it to game class
       game.setWorld(world);
        
       Body background = new Body(world);
       BodyImage backgroundImage = new BodyImage("images/Background.jpeg");
       background.setImage(backgroundImage);
        
        // make the ground
        Body ground = new Body(world, PolygonShape.makeBox(260, 5), Body.Type.STATIC);
        ground.setPosition(new BVec2(-10, -240));
        ground.setImage(new BodyImage("images/Floor.gif"));
        // put myHeli on the ground
        // make a heli
        myHeli = new Helicopter(game, ground);
        //send it to game class
        game.setHeli(myHeli);
        
        myHeli.putOn(ground);
        AttackManager attackM = new AttackManager(myHeli);
        myHeli.setAttackManager(attackM);
   
        Body leftWall = new Body(world, PolygonShape.makeBox(5, 250), Body.Type.STATIC);
        Body rightWall = new Body(world, PolygonShape.makeBox(5, 250), Body.Type.STATIC);
        //and make a cieling
        Body ceiling = new Body(world, PolygonShape.makeBox(500, 5), Body.Type.STATIC);
        
        //apply images
        leftWall.setImage(new BodyImage("images/Wall.gif"));
        rightWall.setImage(new BodyImage("images/Wall.gif"));
        ceiling.setImage(new BodyImage("images/Cieling.gif"));
        
        
        //make walls
        leftWall.setPosition(new BVec2(-245, 0));
        rightWall.setPosition(new BVec2(245, 0));
        ceiling.setPosition(new BVec2(0,245));
        
        //make life harder for player
        Body movingPlatform = new SlidingPlatform(world, PolygonShape.makeBox(67, 10), new BVec2(250, 0), 3.0f);
        Body movingPlatform2 = new SlidingPlatform(world, PolygonShape.makeBox(67, 10), new BVec2(-250, 0), 3.0f);
        Body movingPlatform3 = new SlidingPlatform(world, PolygonShape.makeBox(67, 10), new BVec2(250, 0), 3.0f);
        
        //set images
        movingPlatform.setImage(new BodyImage("images/SlidingPlatform.gif"));
        movingPlatform2.setImage(new BodyImage("images/SlidingPlatform.gif"));
        movingPlatform3.setImage(new BodyImage("images/SlidingPlatform.gif"));
        
        movingPlatform.setPosition(new BVec2(-140, 140));
        movingPlatform2.setPosition(new BVec2(140, 40));
        movingPlatform3.setPosition(new BVec2(-140, -60));

        
        // make some fuel pieces
        CollectableFuel fuel1 = new CollectableFuel(game);
        CollectableFuel fuel2 = new CollectableFuel(game);
        CollectableFuel fuel3 = new CollectableFuel(game);
        CollectableFuel fuel4 = new CollectableFuel(game);
        CollectableFuel fuel5 = new CollectableFuel(game);
        
        //make some time pieces
        CollectableTime time1 = new CollectableTime(game);
        CollectableTime time2 = new CollectableTime(game);
        CollectableTime time3 = new CollectableTime(game);
        CollectableTime time4 = new CollectableTime(game);
        
        //place the fuel places
        fuel1.move(new BVec2(-200, 0));
        fuel2.move(new BVec2(200,0));
        fuel3.move(new BVec2(0,90));
        fuel4.move(new BVec2(200,200));
        fuel5.move(new BVec2(-200,200));
        
        time2.move(new BVec2(200,90));
        time3.move(new BVec2(-200, 90));
        time4.move(new BVec2(0,200));
       
        
        view = game.getView();
        view.setWorld(world);
        
        // start!
        world.start();
    }
    /*
     * destroy level
     * 
     */
    @Override
    void destroy() {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
