package game;

import city.soi.platform.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;

/**
 * My player .. This class makes a helicopter which can shoot missiles, bombs
 * and it can also explode, if you dont take good care of it
 */
public class Helicopter extends Shooter implements CollisionListener, ActionListener, StepListener {

    /**
     * The number of seconds the player currently has.
     */
    private int time;
    private int fuel;
    private boolean exploded;
    private int health;
    /**
     * The game.
     */
    private Game game;
    private boolean flying, arrowDown, flyingRight;
    private int maxSpeed;
    private BodyImage right, left;
    private Shape shapeRight, shapeLeft;
    private Ammunition bugMe;
    private Timer timer;
    private float bounce;
    private Body landing, target, laser, crosshair;
    private AttackManager attackM;

    /**
     * Initialise a new player.
     *
     * @param game The game in which the player will be playing.
     * @param landing - the platform which can be used for docking heli with no
     * damage (collisions ignored)
     */
    //for the sake of extension, the target field can be replaced by a linked list,
    //and then an appropriate handler deep in this class, to help choose and shoot targets
    public Helicopter(Game game, Body landing) {
        //tell super class shooter that only 20 rounds max can be fired
        super(game, 20);
        //get copter_right shape
        //hold shape references for right
        shapeRight = new PolygonShape(14.5f, 18.0f, 50.5f, 13.0f, 29.5f, -15.0f, -0.5f, -15.0f, -56.5f, -4.0f, -56.5f, 6.0f, -21.5f, 13.0f);
        //and  left shapes
        shapeLeft = new PolygonShape(-15.5f, 18.0f, -51.5f, 13.0f, -30.5f, -15.0f, -0.5f, -15.0f, 55.5f, -4.0f, 55.5f, 6.0f, 20.5f, 13.0f);

        //bouncy ratio
        bounce = 0.5f;
        shapeLeft.setRestitution(bounce);
        shapeRight.setRestitution(bounce);

        this.addShape(shapeRight);
        //when heli is created, it hasn't blown up.
        exploded = false;

        this.game = game;

        time = 100;
        fuel = 100;

        this.landing = landing;

        //set images
        //initially copter is facing right
        right = new BodyImage("images/copter.gif");
        left = new BodyImage("images/copter left.gif");

        flyingRight = true;
        setImage(right);

        health = 100;
        game.getWorld().addCollisionListener(this);

        //timer for managing level time, and heli fuel
        timer = new Timer(1000, this);
        timer.start();
        game.getWorld().addStepListener(this);

        //chopper sound woop!
        SoundPlayer.startChopper();

        //laser aiming!
        laser = new Body(game.getWorld(), PolygonShape.makeBox(250f, 1f), Body.Type.STATIC);
        laser.setImage(new BodyImage("images/laser.png"));
        laser.setGhostly(true);

        //cross hair object
        crosshair = new Body(game.getWorld());
        crosshair.addShape(new PolygonShape(-8.5f, 8.5f, -8.5f, -7.5f, 7.5f, -7.5f, 7.5f, 8.5f));
        crosshair.setImage(new BodyImage("images/crosshair.png"));
        crosshair.setGhostly(true);
        crosshair.setRenderLayer(2);
        crosshair.setGravityStrength(0f);
        crosshair.setAngularVelocity(3);
    }

    /**
     * stop keeping track of time and fuel
     */
    public void stopTimer() {
        timer.stop();
    }

    /**
     * start keeping track of time and fuel
     */
    public void startTimer() {
        timer.start();
    }

    /**
     * Initiate flying
     *
     * @param fly
     */
    public void setFlying(boolean fly) {
        this.flying = fly;
        if (flying == true && !exploded) {
            //play the sound effects
            SoundPlayer.restartChopper();
        } else if (!exploded) {
            //stop playing the sound effects, if exploded
            SoundPlayer.stopChopper();
        }
    }

    /**
     * set fuel
     *
     * @param fuel
     */
    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    /**
     * get fuel
     *
     * @return
     */
    public int getFuel() {
        return this.fuel;
    }

    /**
     * increase the fuel count.
     */
    public void incrementFuel() {
        fuel += 10;
    }

    /**
     * The number of seconds the player currently has.
     *
     * @return
     */
    public int getTime() {
        return time;
    }

    /**
     * set time
     *
     * @param t
     */
    public void setTime(int t) {
        this.time = t;
    }

    /**
     * get health
     *
     * @return
     */
    public int getHealth() {
        return health;
    }

    /**
     * set health
     *
     * @param h
     */
    public void setHealth(int h) {
        health = h;
    }
    //called when a time piece is collected

    /**
     * increase time by 100 seconds
     */
    public void incrementTime() {
        this.time += 100;
    }

    /**
     * decrement time.
     */
    public void decrementTime() {
        this.time--;
    }

    /**
     * decrement fuel - only when flying
     */
    public void decrementFuel() {
        this.fuel--;
    }

    /**
     * print status to console
     */
    public void printStatus() {
        if (time < 0) {
            time = 0;
        }
        if (fuel < 0) {
            fuel = 0;
        }
        //console outdated
        // System.out.println("Time: " + time + " Fuel: " + fuel + " Health: " + this.health);
        //use GUI instead
        game.updateGUI();
    }

    /**
     * flying time, called by step listener every so often
     */
    public void fly() {
        //if fuel and time resources are available
        if (fuel + time > 0) {
            //rotate the flight vector
            BVec2 rotated = new BVec2(0, 40);
            rotated.rotate(this.getAngle());
            //apply as an impulse
            this.applyImpulse(rotated);
        }
    }

    /**
     * stop flying and fall!
     */
    public void stopFlying() {
        setFlying(false);
    }

    /**
     * keep track of held keys
     *
     * @param arrow
     */
    public void setArrowDown(boolean arrow) {
        this.arrowDown = arrow;
    }

    /**
     * lean left, tilt
     */
    public void leanLeft() {
        //change image, if facing the wrong way
        if (this.flyingRight) {
            this.setImage(left);
            //change direction
            this.flyingRight = false;
            //and change the shape of object;
            shapeLeft = new PolygonShape(-15.5f, 18.0f, -51.5f, 13.0f, -30.5f, -15.0f, -0.5f, -15.0f, 55.5f, -4.0f, 55.5f, 6.0f, 20.5f, 13.0f);
            shapeLeft.setRestitution(bounce);

            this.addShape(shapeLeft);
            //and remove other shape
            this.removeShape(shapeRight);
        }
        //key is held down
        this.arrowDown = true;

    }

    /**
     * set attack manager
     *
     * @param attackM
     */
    public void setAttackManager(AttackManager attackM) {
        this.attackM = attackM;
    }

    /**
     * get attack manager reference
     *
     * @return
     */
    public AttackManager getAttackManager() {
        return attackM;
    }

    /**
     * lean right, tilt
     */
    public void leanRight() {
        //change shape and image if facig the wrong way
        if (!this.flyingRight) {
            this.setImage(right);
            //change direction
            this.flyingRight = true;
            //change shape of object
            shapeRight = new PolygonShape(14.5f, 18.0f, 50.5f, 13.0f, 29.5f, -15.0f, -0.5f, -15.0f, -56.5f, -4.0f, -56.5f, 6.0f, -21.5f, 13.0f);
            shapeRight.setRestitution(bounce);
            this.addShape(shapeRight);


            //and remove other shape
            this.removeShape(shapeLeft);
        }
        //key is held down
        this.arrowDown = true;
    }

    /**
     * fly horizontal
     */
    public void flyHorizontal() {
        //flying right
        if (flyingRight && arrowDown) {
            if (fuel > 0 && time > 0) {
                this.applyTorque(-3000);
            }
        } // fly left
        else if (!flyingRight && arrowDown) {
            if (fuel > 0 && time > 0) {
                this.applyTorque(3000);
            }
        } //stabalisation code
        else if (Math.abs(this.getAngle()) < Math.PI) {
            this.setAngularVelocity(-(float) this.getAngle());
        } else {
            this.setAngularVelocity((float) this.getAngle());
        }
        if (Math.abs(this.getAngle()) > 2 * Math.PI) {
            this.setAngle((float) (this.getAngle() % Math.PI));
        }

    }

    /**
     * in case of a collision
     *
     * @param collided
     */
    public void collide(CollisionEvent collided) {
        Body hitme = collided.getOtherBody();
        //decrease health if not hit by landing platform, a bomb, missile or a missile trail
        if (!hitme.equals(landing) && !(hitme instanceof Bomb) && !(hitme instanceof Trail) && !(hitme instanceof Missile)) {
            this.health--;
        }
        //update status
        printStatus();
        //if hit by canonball
        if (hitme instanceof Cannonball) {
            //clunk!
            SoundPlayer.play("characterHit", 0.5f);
        }
        if (this.health < 0) {
            //if too weak, blow up and die
            explode();
        }
    }

    /**
     * get exploded
     *
     * @return
     */
    public boolean getExploded() {
        return this.exploded;
    }

    /**
     * draw laser pointer, only when there is no target
     */
    public void drawLaser() {
        Body target = attackM.getCurrentTarget();
        BVec2 aim = null;
        if(target!=null) {
            aim= new BVec2(target.getPosition());
            aim.addLocal(this.getPosition());    
        }
        else {
            if(this.flyingRight) aim = new BVec2(260,-20);
            else  aim = new BVec2(-260,-20);
            aim.addLocal(this.getPosition());
            aim.rotateAboutPoint(this.getPosition(),this.getAngle());
            laser.setPosition(aim);
            laser.setAngle(this.getAngle());
        }
        
    }

    /**
     * control laser visibility to do- change laser colour
     *
     * @param visible
     */
    public void setLaserVisibility(boolean visible) {
        laser.setVisible(visible);
    }

    /**
     * explode!
     */
    public void explode() {
        //if death occurs on credits, don't insult the player
        //otherwise show game over message
        if (game.getLevel() != 4) {
            Body overlay = new Body(game.getWorld(), PolygonShape.makeBox(50f, 50f), Body.Type.STATIC);
            overlay.setGhostly(true);
            overlay.setImage(new BodyImage("images/Gameover.png"));
            overlay.setRenderLayer(0);
        }
        //invisible laser
        laser.setVisible(false);

        this.destroy();
        this.setName("destroyed");
        this.exploded = true;
        SoundPlayer.play("characterExplosion", 0.5f);
        //stop chopper sounds
        SoundPlayer.killChopper();

        //hide the cross hair
        crosshair.setVisible(false);

        //make shapes
        PolygonShape cockpit = new PolygonShape(-12.0f, -3.5f, 2.0f, -3.5f, 9.0f, -2.5f, 11.0f, -0.5f, 11.0f, 2.5f, 9.0f, 4.5f, 2.0f, 4.5f, -12.0f, 4.5f);
        PolygonShape toprotor = new PolygonShape(-36.5f, 1.5f, -36.5f, -0.5f, 35.5f, -0.5f, 35.5f, 1.5f);
        PolygonShape spindle = new PolygonShape(-1.5f, 4.0f, -1.5f, -3.0f, 0.5f, -3.0f, 0.5f, 4.0f);
        PolygonShape equals1 = new PolygonShape(-2.5f, 1.5f, -2.5f, -0.5f, 1.5f, -0.5f, 1.5f, 1.5f);
        PolygonShape equals2 = new PolygonShape(-2.5f, 1.5f, -2.5f, -0.5f, 1.5f, -0.5f, 1.5f, 1.5f);
        PolygonShape equals3 = new PolygonShape(-2.5f, 1.5f, -2.5f, -0.5f, 1.5f, -0.5f, 1.5f, 1.5f);
        PolygonShape equals4 = new PolygonShape(-2.5f, 1.5f, -2.5f, -0.5f, 1.5f, -0.5f, 1.5f, 1.5f);
        PolygonShape equals5 = new PolygonShape(-2.5f, 1.5f, -2.5f, -0.5f, 1.5f, -0.5f, 1.5f, 1.5f);
        PolygonShape minus1 = new PolygonShape(-1.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f);
        PolygonShape minus2 = new PolygonShape(-1.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f);
        PolygonShape minus3 = new PolygonShape(-1.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f);
        PolygonShape minus4 = new PolygonShape(-1.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f);
        PolygonShape minus5 = new PolygonShape(-1.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f);
        PolygonShape minus6 = new PolygonShape(-1.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f);
        PolygonShape minus7 = new PolygonShape(-1.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f);
        PolygonShape semi = new PolygonShape(0.0f, 3.5f, -1.0f, -2.5f, 0.0f, -1.5f);
        PolygonShape morethan = new PolygonShape(-2.0f, 2.5f, 1.0f, 0.5f, -2.0f, -1.5f);
        PolygonShape bracket = new PolygonShape(0.5f, 4.5f, -1.5f, 2.5f, -1.5f, -1.5f, 0.5f, -3.5f);
        PolygonShape pipe = new PolygonShape(-0.5f, -3.0f, 0.5f, -3.0f, 0.5f, 4.0f, -0.5f, 4.0f);

        //make bodies
        Body bcockpit = new Body(game.getWorld(), cockpit, Body.Type.DYNAMIC);
        Body btoprotor = new Body(game.getWorld(), toprotor, Body.Type.DYNAMIC);
        Body bspindle = new Body(game.getWorld(), spindle, Body.Type.DYNAMIC);
        Body bequals1 = new Body(game.getWorld(), equals1, Body.Type.DYNAMIC);
        Body bequals2 = new Body(game.getWorld(), equals2, Body.Type.DYNAMIC);
        Body bequals3 = new Body(game.getWorld(), equals3, Body.Type.DYNAMIC);
        Body bequals4 = new Body(game.getWorld(), equals4, Body.Type.DYNAMIC);
        Body bequals5 = new Body(game.getWorld(), equals5, Body.Type.DYNAMIC);
        Body bminus1 = new Body(game.getWorld(), minus1, Body.Type.DYNAMIC);
        Body bminus2 = new Body(game.getWorld(), minus2, Body.Type.DYNAMIC);
        Body bminus3 = new Body(game.getWorld(), minus3, Body.Type.DYNAMIC);
        Body bminus4 = new Body(game.getWorld(), minus4, Body.Type.DYNAMIC);
        Body bminus5 = new Body(game.getWorld(), minus5, Body.Type.DYNAMIC);
        Body bminus6 = new Body(game.getWorld(), minus6, Body.Type.DYNAMIC);
        Body bsemi = new Body(game.getWorld(), semi, Body.Type.DYNAMIC);
        Body bmorethan = new Body(game.getWorld(), morethan, Body.Type.DYNAMIC);
        Body bbracket = new Body(game.getWorld(), bracket, Body.Type.DYNAMIC);
        Body bpipe = new Body(game.getWorld(), pipe, Body.Type.DYNAMIC);

        //set images
        bpipe.setImage(new BodyImage("images/pipe.gif"));
        bbracket.setImage(new BodyImage("images/bracket.gif"));
        bmorethan.setImage(new BodyImage("images/morethan.gif"));
        bsemi.setImage(new BodyImage("images/semi.gif"));
        bminus6.setImage(new BodyImage("images/minus.gif"));
        bminus5.setImage(new BodyImage("images/minus.gif"));
        bminus4.setImage(new BodyImage("images/minus.gif"));
        bminus3.setImage(new BodyImage("images/minus.gif"));
        bminus2.setImage(new BodyImage("images/minus.gif"));
        bminus1.setImage(new BodyImage("images/minus.gif"));
        bequals5.setImage(new BodyImage("images/equals.gif"));
        bequals4.setImage(new BodyImage("images/equals.gif"));
        bequals3.setImage(new BodyImage("images/equals.gif"));
        bequals2.setImage(new BodyImage("images/equals.gif"));
        bequals1.setImage(new BodyImage("images/equals.gif"));
        bspindle.setImage(new BodyImage("images/rotor_I.gif"));
        btoprotor.setImage(new BodyImage("images/top_rotor.gif"));
        bcockpit.setImage(new BodyImage("images/cockpit.gif"));

        //position evert fragment
        bcockpit.setPosition(new BVec2(2, 2));
        btoprotor.setPosition(new BVec2(-5, 20));
        bspindle.setPosition(new BVec2(-3, 20));
        bequals1.setPosition(new BVec2(-20, 5));
        bequals2.setPosition(new BVec2(-28, 5));
        bequals3.setPosition(new BVec2(-36, 5));
        bequals4.setPosition(new BVec2(-44, 5));
        bequals5.setPosition(new BVec2(-52, 5));
        bsemi.setPosition(new BVec2(-58, 5));
        bmorethan.setPosition(new BVec2(-63, 5));
        bpipe.setPosition(new BVec2(-69, 5));
        bbracket.setPosition(new BVec2(-75, 5));
        bminus1.setPosition(new BVec2(-12, -15));
        bminus2.setPosition(new BVec2(-6, -15));
        bminus3.setPosition(new BVec2(0, -15));
        bminus4.setPosition(new BVec2(6, -15));
        bminus5.setPosition(new BVec2(12, -15));
        bminus6.setPosition(new BVec2(-5, -10));
        bminus6.setAngle(90);

        //add it to an array
        ArrayList<Body> copterPieces = new ArrayList<Body>();
        copterPieces.add(bpipe);
        copterPieces.add(bequals1);
        copterPieces.add(bequals2);
        copterPieces.add(bequals3);
        copterPieces.add(bequals4);
        copterPieces.add(bequals5);
        copterPieces.add(bminus1);
        copterPieces.add(bminus2);
        copterPieces.add(bminus3);
        copterPieces.add(bminus4);
        copterPieces.add(bminus5);
        copterPieces.add(bminus6);
        copterPieces.add(btoprotor);
        copterPieces.add(bspindle);
        copterPieces.add(bsemi);
        copterPieces.add(bmorethan);
        copterPieces.add(bbracket);
        copterPieces.add(bcockpit);

        //current speed, with direction
        BVec2 currentVelocity = new BVec2(this.getLinearVelocity());

        //blow every piece away
        for (Body piece : copterPieces) {
            //translation and rotation happens here
            BVec2 center = new BVec2(this.getPosition());
            float angle = this.getAngle();

            //if flying right, flip
            if (!this.flyingRight) {
                float pieceAngle = (float) (angle + Math.PI);
                BVec2 newPos = new BVec2(piece.getPosition());
                newPos.x = -1 * newPos.x;
                center.set(center.add(newPos));
                piece.setAngle((float) pieceAngle);
            } else {
                //otherwise don't flip
                center.set(center.add(piece.getPosition()));
                piece.setAngle((float) angle);
            }
            //rotation around center,
            center.rotateAboutPoint(getPosition(), angle);
            //reposition
            piece.setPosition(center);
            //get a direction vector
            BVec2 direction = center.sub(getPosition());

            //normalise with a resultant length of 100
            float scale = 100 / direction.length();
            direction.set(direction.mul(scale));

            //and blow it all away
            piece.setLinearVelocity(direction);
            piece.setAngularVelocity(this.getAngularVelocity());
        }

    }

    /**
     * timer events land here
     *
     * @param acEv
     */
    @Override
    public void actionPerformed(ActionEvent acEv) {
        //if flying, use fuel
        if (this.flying) {
            this.decrementFuel();
        }
        //decrement time
        this.decrementTime();
        //update GUI
        this.printStatus();
    }
    /*
     *shoot ammo
     */

    @Override
    void shootAmmo(String choice) {
        //only if not exploded
        if (!exploded) {
            //missile?
            if (choice == "Missile") {
                bugMe = new Missile(game, this, this, attackM);
            //or would you like a bomb?
            } else if (choice == "Bomb") {
                bugMe = new Bomb(game, this);
            }
        }
    }

    /**
     * Cross hair is placed on target. Could be an enemy, or could be a piece of enemy ammo (counter attack mode)
     */
    public void placeCrossHair() {
        //get target from attack manager
        Body target = attackM.getCurrentTarget();
        
        if (target != null) {
            BVec2 aim = new BVec2(target.getPosition());
            crosshair.setPosition(aim);
            //cant see laser on heat seeking
            laser.setVisible(false);
            crosshair.setVisible(true);
        } else {
            //can see laser pointer, if there are no more targets
            laser.setVisible(true);
            
            crosshair.setVisible(false);
        }
    }

    /**
     *step listener, gives smooth control on heli features
     * @param se
     */
    @Override
    public void preStep(StepEvent se) {
        if (this.flying) {
            //fly, apply impulses
            this.fly();
        }
        //stabilise or tilt
        this.flyHorizontal();
        placeCrossHair();
        drawLaser();
    }

    /**
     *Not used,stub
     * @param se
     */
    @Override
    public void postStep(StepEvent se) {
    }

    /**
     *return direction heli is facing
     * @return
     */
    public boolean getDirection() {
        return flyingRight;
    }
}
