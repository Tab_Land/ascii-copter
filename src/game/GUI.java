/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author ijtaba
 * GUI panel,
 * holds buttons for restarting/ pausing game/ god mode
 * and alerts user of the fuel left, the time left, and health status
 */
public class GUI extends JPanel implements ActionListener{
    private Game game;
    private JLabel time, fuel,health;
    private JButton pause, godMode, restart;
    
    /**
     * create a new gui panel
     * @param game - game ref
     */
    public GUI(Game game){
        super(new BorderLayout());
        
        
        //boiler plate text
        time =  new JLabel("Time: 100");
        fuel = new JLabel("Fuel: 100");
        health = new JLabel("Health: 100");
        pause = new JButton("Pause");
        restart = new JButton("Restart");
        godMode = new JButton("God Mode");
        
        
        this.game = game;
        //if focusable is not set again and again, mouse and keyboard events are stolen from game
        pause.setFocusable(false);
        godMode.setFocusable(false);
        restart.setFocusable(false);
        this.setFocusable(false);
        
        pause.addActionListener(this);
        godMode.addActionListener(this);
        restart.addActionListener(this);
        
        pause.setActionCommand("pause");
        godMode.setActionCommand("godModeOn");
        restart.setActionCommand("restart");
        
        //break up into two rows
        JPanel firstRow = new JPanel(new FlowLayout());
        JPanel secondRow = new JPanel(new FlowLayout());
        
        //fancy appearance stuff
        this.setBackground(Color.white);
        pause.setBackground(Color.white);
        //Unlimited fuel, time and health cheat
        godMode.setBackground(Color.white);
        restart.setBackground(Color.white);
        firstRow.setBackground(Color.white);
        secondRow.setBackground(Color.white);
        
        //first row holds status labels
        firstRow.add(time);
        firstRow.add(fuel);
        firstRow.add(health);
        
        
        //second row holds buttons
        secondRow.add(pause);
        secondRow.add(godMode);
        secondRow.add(restart);
        
        
        this.add(firstRow, BorderLayout.NORTH);
        this.add(secondRow,BorderLayout.SOUTH);
        
    }
    /**
     * update time on GUI
     * @param newTime - String
     */
    public void setTime(String newTime){
        time.setText("Time: " + newTime);
    }
    /**
     * update health on GUI
     * @param newHealth - String
     */
    public void setHealth(String newHealth){
        health.setText("Health: " + newHealth);
    }
    /**
     * update fuel on GUI
     * @param newFuel - String
     */
    public void setFuel(String newFuel){
        fuel.setText("Fuel: " + newFuel);
    }
    /**
     * pause the game - called by pause button
     */
    public void pause(){
        game.pauseGame();
        //return focus to game
        game.resetFocus();
        //change the pause buttons action and text
        pause.setActionCommand("unpause");
        pause.setText("Unpause");
    }
    /**
     * caused by unpause button
     */
    public void unpause(){
        //unpause
        game.unpauseGame();
        //return focus to game
        game.resetFocus();
        //reconfigure this button back
        pause.setActionCommand("pause");
        pause.setText("Pause");
    }
    /**
     * Turn on god mode
     */
    public void godModeOn(){
        game.godMode();
    }

    /**
     * all button actions come to this method first
     * @param acEv
     */
    @Override
    public void actionPerformed(ActionEvent acEv) {
        String Action;
        //filtring string
        Action = acEv.getActionCommand();
        //if pause - pause game
        if(Action.equals("pause")){
            this.pause();
        }
        //if god mode - turn on god mode
        else if(Action.equals("godModeOn")){
            this.godModeOn();
            //godMode.setActionCommand("godModeOff");
        }
        //if unpause - unpause game
        else if(Action.equals("unpause")){
            this.unpause();
        }
        //toggle god mode button - doesn't do anything useful really
        else if(Action.equals("godModeOff")){
            //this.godModeOff();
            godMode.setActionCommand("godModeOn");
        }
        //restart the level
        else if(Action.equals("restart")){
            game.restartLevel();
        }
    }
    
}
