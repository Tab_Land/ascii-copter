package game;

import city.soi.platform.*;

/**
 * Pick-ups in a game. When the player collides with a fuel object, the
 * player's fuel count is increased and the fuel object is removed
 * from the world. 
 */
public class CollectableFuel extends Body implements CollisionListener{

    /** The game in which the player is playing. */
    private Game game;

    /**
     * Initialise a new fuel pick up.
     * @param game - game class reference
     */
    public CollectableFuel(Game game) {
        super(game.getWorld(), new PolygonShape(-3.0f,-7.5f, 3.0f,-7.5f, 4.0f,-5.5f, 4.0f,6.5f, 3.0f,8.5f, -3.0f,8.5f, -4.0f,6.5f, -4.0f,-5.5f));
        this.game = game;
        //set images
        this.setImage(new BodyImage("images/Fuel.gif"));
        //attach collision listener
        game.getWorld().addCollisionListener(this);
        //must not fall
        this.setGravityStrength(0);
    }
    
    /**
     * When collided, destroy this pickup, make sound, and add to the players status
     * @param collided
     */
    @Override
    public void collide(CollisionEvent collided){
           Helicopter heliref = game.getHeli();
           if(collided.getOtherBody().equals(heliref)){
               this.destroy();
               //Fuel up!
               heliref.incrementFuel();
               //score board update
               heliref.printStatus();
               //objective completed - objectives help trigger new levels
               game.objectiveCompleted();
               //play fueling sound
               SoundPlayer.play("FuelJet",8f);
           }
    
    }
    
}
