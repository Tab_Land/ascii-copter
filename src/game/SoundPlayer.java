/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.SoundClip;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Timer;

/**
 *SoundManagement class
 * This class is very buggy,
 * all method implementations have been commented out so game can continue playing
 * @author ijtaba
 */
public class SoundPlayer {

    //flying sound effect for the chopper
    private static SoundClip chopper;
    //if flying we play a louder chopper sound effect and vice versa
    private static boolean flying;
    // a field to hold a sound clip temporarily, until it is dispatched back to the class which asked for it to be played
    private static SoundClip sound;

    /**
     * static play method
     * a file name , and a volume is provided 
     * and this method plays said sound file in a thread
     * it also returns said sound bite for finer control by other classes
     * @param file
     * @param volume
     * @return
     */
    public static SoundClip play(final String file, final double volume) {/*
        // create new sound in new thread
        Runnable run = new Runnable() {
           
            @Override
            public void run() {
                
                try {
                    //make and play soundclip
                    sound = new SoundClip("sounds/" + file + ".wav");
                    sound.setVolume(volume);
                    sound.play();
                    //allt the things that could go wrong
                } catch (UnsupportedAudioFileException ex) {
                    Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
                } catch (LineUnavailableException ex) {
                    //Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("audio line busy");
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        Thread thread = new Thread(run);
        thread.start();
        //return soundclip for finer control
        
        return sound;*/
        return null;
    }

    /**
     * lower the chopper volume, its not flying, less thrust
     */
    public static void stopChopper() {
        /*if (flying) {
        if(chopper!=null) chopper.setVolume(0.2f);
            flying = false;
        }*/
    }

    /**
     * stop playing chopper sound all together
     */
    public static void killChopper() {/*
        if (chopper != null) {
            //chopper.close();
            chopper = null;
        }*/
    }

    /**
     * restart the chopper sound effect. Useful for new levels, 
     */
    public static void restartChopper() {/*
        if (!flying) {
           if(chopper!=null) chopper.setVolume(0.5f);
            flying = true;
        }*/

    }

    /**
     * start playing the chopper clip
     */
    public static void startChopper() {/*
        if (chopper == null) {
            try {
                chopper = new SoundClip("sounds/chopper.wav");
                chopper.setVolume(0.1f);
                //small soundbite is looped for memory management
                chopper.loop();
            } catch (UnsupportedAudioFileException ex) {
                Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (LineUnavailableException ex) {
                Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
    }
}
