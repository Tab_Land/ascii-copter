/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.Body;
import city.soi.platform.BodyImage;
import city.soi.platform.PolygonShape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 * Animated and elegant trailing asterisks
 * @author ijtaba
 */
    
public class Trail extends Body implements ActionListener{
    private Timer timer;
    private int frame;
    /**
     * create a new trail
     * @param game
     */
    public Trail(Game game){
        super(game.getWorld());
        
        //behave like a flare, gravity is low
        
        this.setGravityStrength(0.01f);
        this.setGhostly(true);
        
        this.setImage(new BodyImage("images/asterik.png"));
        this.addShape(new PolygonShape(3.0f,4.0f, -4.0f,4.0f, -4.0f,-3.0f, 3.0f,-3.0f));
        //framecounter is used to keep track of when the image should be changed(for animation)
        //and eventually when the trail should be destroyed
        frame = 2;
        timer = new Timer(200, this);
        timer.start();
    }

    /**
     * Timer calls this method. 
     * used for animation fade, and then the trail particle is destroyed
     * @param ae
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(frame==5) destroyTrail();
        else {
            //there are about 5 images, in different degrees of transparency
            //changing the pictures every 200ms gives a very smooth fading effect
             this.setImage(new BodyImage("images/asterik"+frame+".png"));
             frame++;
        }
    }

    /**
     * destroy this particle of trail smoke. Timer is released too
     */
    public void destroyTrail(){
        timer.stop();
        this.destroy();
    }
}
