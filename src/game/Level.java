/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.Body;
import city.soi.platform.World;
import city.soi.platform.WorldView;
import java.util.LinkedList;

/**
 *
 * @author ijtaba
 */

//provides handy functions for getting fields back, and handling memory 
public abstract class Level {
    private WorldView view;
    private World world;
    
    /**
     * 
     * @return
     */
    public WorldView getView(){
        return view;
    }
    /**
     * 
     * @return
     */
    public World getWorld(){
        return world;
    }
    abstract void destroy();
    
    
}
