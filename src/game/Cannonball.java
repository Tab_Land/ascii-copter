/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.Body;
import city.soi.platform.BodyImage;
import city.soi.platform.CircleShape;


/**
 *
 * @author ijtaba
 * Cannonball
 * a type of munition which a tank fires
 */
public class Cannonball extends Ammunition{
    private Tank tankref;
    private AttackManager attackM;
    
    /**
     * Create new Cannonball
     * @param game - game reference
     * @param tankref - which tank fired it
     * @param target - who to fire at?
     */
    public Cannonball(Game game, Tank tankref, Body target){
        super(game, new CircleShape(5f), tankref, 1200, target);
        this.tankref = tankref;
        //set position to the canon edge, looks better
        this.setPosition(tankref.getCannonEdge());
        //and shoot!
        shootAmmunition();
    }
    
    /**
     * Sed images
     */
    @Override
    public void setBody(){
            //use this function to set bodyshape and image
        this.setImage(new BodyImage("images/Cannonball.png"));
        
    }
    /**
     * No need to rotate a canonball, stub method from ammunition
     */
    @Override
    public void setRotation(){
    //doesn't need to do anything   
    }
    /**
     * Set attack manager
     * the canon ball holds a reference to this, to remove itself when destroyed
     * @param attackM
     */
    public void setAttackManager(AttackManager attackM) {
        this.attackM = attackM;
    }
    /*
     * destroy ball , and free memory
     * 
     */
    @Override
    public void destroyAmmo(){
        super.destroyAmmo();
        attackM.removeAmmo(this);
    }
    /*
     * A canonball has no fragments, stub method from munitions class
     */
    @Override
    void explode() {
        //not needed
    }
}
