/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;

/**
 *Missile type of ammunition
 * Heat seeking
 * works with the attack manager to attack enemies, or counter attack their ammunition
 * @author ijtaba
 */
public class Missile extends Ammunition implements StepListener, CollisionListener, ActionListener {

    private Helicopter heliref;
    private Game game;
    private float x, y;
    private int trailCount;
    private ArrayList<Body> pieces;
    private BVec2 postKillDirection;
    private SoundClip launch;
    private AttackManager attackM;

    /**
     * create missile
     * @param game - game reference
     * @param shooter - shooter reference (heli)
     * @param heliref - helicopter reference
     * @param attackM - attack manager
     */
    public Missile(Game game, Shooter shooter, Helicopter heliref, AttackManager attackM) {
        super(game, new PolygonShape(-9.5f, 2.5f, -14.5f, 0.5f, -9.5f, -1.5f, 13.5f, -1.5f, 13.5f, 2.5f), shooter, 60000, attackM.getCurrentTarget());
        this.heliref = heliref;
        this.target = attackM.getCurrentTarget();
        setRotation();
        this.setGravityStrength(0);
        //this.setPosition( shooter.getCannonEdge());
        this.attackM = attackM;

        shootAmmunition();
        this.game = game;
        game.getWorld().addStepListener(this);
        game.getWorld().addCollisionListener(this);

        //missile launch sound
        launch = SoundPlayer.play("missileLaunch", 0.3f);
    }

    /**
     * 
     */
    @Override
    public void setBody() {
        //use this function to set bodyshape and image
        this.setImage(new BodyImage("images/Bomb.gif"));
        this.setPosition(getShooter().getPosition().add(new BVec2(0, -20)));
    }
    //should I create a trail of fog behind rocket? - Hell yes!

    /**
     * Trail creation. asterisks go blazing at regular time intervals
     * and fade away gradually
     * provide cover against canonballs
     */
    public void createTrail() {
        if (game != null) {
            BVec2 offset = new BVec2(3, 0);
            offset.rotate(this.getAngle());
            Trail trail = new Trail(game);
            trail.setPosition(this.getPosition().add(offset));
            //trailcount controls the rate of new trails
            //increase for a less trail
            //keep near zero for rich trailing
            trailCount = 0;
        }
    }

    /**
     * rotate missile, always points towards the target
     */
    @Override
    public void setRotation() {
        BVec2 curpos = new BVec2(this.getPosition());
        //get initial position
        
        float angle = 0;
        if (target != null) {
            //get angle of missile to target vector, if target exists
            angle = (float) curpos.getAngleFromPoint(target.getPosition());
        } else {
            //if target does not exist
            boolean direction = true;
            if (heliref != null) {
                //point in the helis direction
                direction = heliref.getDirection();
            }
            if (!direction) {
                angle = (float) getShooter().getAngle();
            } else {
                angle = (float) (getShooter().getAngle() + Math.PI);
            }
        }
            this.setAngle(angle);
       
    }
    /*
     * shoot this misile
     */
    @Override
    public void shootAmmunition() {
        //update target
        this.target = attackM.getCurrentTarget();
        //get its coordinates
        getCoordinates();
        //map direction to it
        BVec2 direction = new BVec2(targetx - x, targety - y);
        //normalise with a length of 10
        direction.mulLocal(10 / direction.length());
        //apply new direction as an impulse
        this.applyImpulse(direction);
        //update rotation (point to target)
        setRotation();
        if (trailCount == 0) {
            //create trail if trail count meets the criteria
            createTrail();
        } else {
            trailCount--;
        }
    }

    /**
     * step listener helps coordinate the missile
     * impulses push it to the target
     * and rotate toward the target
     * @param e
     */
    @Override
    public void preStep(StepEvent e) {
        shootAmmunition();
    }

    /**
     * stub method, no need to use
     * @param e
     */
    @Override
    public void postStep(StepEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * collision handling done here
     * @param e
     */
    @Override
    public void collide(CollisionEvent e) {
        Body other = e.getOtherBody();
        //if target body hit, destroy ammo
        //ignore touches with trail and helicopters, and other instances of missiles

        if (other instanceof Cannonball) {
            BVec2 direction = new BVec2(this.getLinearVelocity());
            direction.mulLocal(50/direction.length());
            other.applyImpulse(direction);
        }

        if (!(other instanceof Trail) && !(other instanceof Helicopter) && !(other instanceof Missile)) {
            destroyAmmo();
        }


    }
    //This version gets coordinates of missile and target, not shooter and target.

    @Override
    public void getCoordinates() {
        x = getPosition().x;
        y = getPosition().y;
        //shooterx should be renamed target x
        if (target != null) {
            targetx = target.getPosition().x;
            targety = target.getPosition().y;
            if ("destroyed".equals(target.getName()) && timer != null) {
                target = null;
            }
        } else {
            //need to think about helicopter direction
            boolean direction = true;
            if (heliref != null) {
                direction = heliref.getDirection();
            }
            if (direction) {
                postKillDirection = new BVec2(-3000, 0);
            } else {
                postKillDirection = new BVec2(3000, 0);
            }
            postKillDirection.rotate(getShooter().getAngle() + Math.PI);
            postKillDirection.addLocal(getShooter().getPosition());
            targetx = postKillDirection.x;
            targety = postKillDirection.y;
        }
    }
    /*
     * missile has been badly used up
     * time to destroy and free memory
     */
    @Override
    public void destroyAmmo() {
        //cancel launch (sound)
        if (launch != null) {
            launch.close();
        }
        //play cheesy sound
        SoundPlayer.play("missileExplode", 0.3f);
        super.destroyAmmo();
        if (game != null) {
            //blow into 4 fragments
            explode();
        }
    }
    /*
     * show the fireworks
     * 4 fragments
     * + a hell of a lot of damage
     */
    @Override
    void explode() {
        //create shapes
        PolygonShape morethan = new PolygonShape(-2.0f, 2.5f, 1.0f, 0.5f, -2.0f, -1.5f);
        PolygonShape morethan2 = new PolygonShape(-2.0f, 2.5f, 1.0f, 0.5f, -2.0f, -1.5f);
        PolygonShape equals = new PolygonShape(-2.5f, 1.5f, -2.5f, -0.5f, 1.5f, -0.5f, 1.5f, 1.5f);
        PolygonShape equals2 = new PolygonShape(-2.5f, 1.5f, -2.5f, -0.5f, 1.5f, -0.5f, 1.5f, 1.5f);

        //create bodies
        Body bequals = new Body(game.getWorld(), equals);
        Body bequals2 = new Body(game.getWorld(), equals2);
        Body bmorethan = new Body(game.getWorld(), morethan);
        Body bmorethan2 = new Body(game.getWorld(), morethan2);

        //set images
        bequals.setImage(new BodyImage("images/equals.gif"));
        bequals2.setImage(new BodyImage("images/equals.gif"));
        bmorethan.setImage(new BodyImage("images/morethan.gif"));
        bmorethan.setImage(new BodyImage("images/morethan.gif"));

        //set positions
        bmorethan.setPosition(new BVec2(0, 0));
        bequals.setPosition(new BVec2(6, 0));
        bequals2.setPosition(new BVec2(12, 0));
        bmorethan2.setPosition(new BVec2(18, 0));

        //more than equal to signs to point with the missile
        bmorethan.setAngle((float) Math.PI);
        bmorethan2.setAngle((float) Math.PI);

        //fragment array
        pieces = new ArrayList<Body>();
        pieces.add(bmorethan);
        pieces.add(bmorethan2);
        pieces.add(bequals);
        pieces.add(bequals2);

        //for each fragment
        for (Body piece : pieces) {
            //get center of missile
            BVec2 center = new BVec2(this.getPosition());
            //add offset to center
            piece.setPosition(piece.getPosition().add(center));
            //set angle for piece
            piece.setAngle(this.getAngle() + piece.getAngle());
            //copy angular velocity of missile to fragment
            piece.setAngularVelocity(this.getAngularVelocity());
            //copy linear velocity of missile to fragment
            
            piece.setLinearVelocity(getLinearVelocity());
        }
        
        //after 3 seconds, start clearing fragments
        timer = new Timer(3000, this);
        timer.setActionCommand("clearFragments");
        timer.start();
    }
    /*
     * when timer expires
     */
    @Override
    public void actionPerformed(ActionEvent acEv) {
        if ("clearFragments".equals(acEv.getActionCommand())) {
            //get rid of shards
            clearFragments();
        } else {
            //or destroy overdue ammo
            destroyAmmo();
        }
        timer.stop();
    }
    /*
     * delete all pieces of shrapnel
     */
    private void clearFragments() {
        for (Body piece : pieces) {
            piece.destroy();
        }
    }
}
