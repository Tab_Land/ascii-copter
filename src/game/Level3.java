/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.*;

/**
 * Last level, before credits
 * Aim of the game, destroy the two tanks to finish the copter game
 * @author ijtaba
 */
public class Level3 extends Level{

    private Helicopter myHeli;
    //constructor initialises level
    private World world;
    private WorldView view;
    private Tank tank1, tank2;
    /**
     * create level
     * @param game
     */
    public Level3(Game game){
       //make new world
       world = new World(); 
       //send it to game class
       game.setWorld(world);
        
       Body background = new Body(world);
       background.setImage(new BodyImage("images/Background3.gif"));
        
        // make the ground
        Body ground = new Body(world, PolygonShape.makeBox(260, 5), Body.Type.STATIC);
        ground.setPosition(new BVec2(-10, -240));
        ground.setImage(new BodyImage("images/Floor.gif"));
        // put myHeli on the ground
        

        Body leftWall = new Body(world, PolygonShape.makeBox(5, 250), Body.Type.STATIC);
        Body rightWall = new Body(world, PolygonShape.makeBox(5, 250), Body.Type.STATIC);
        //and make a cieling
        Body ceiling = new Body(world, PolygonShape.makeBox(500, 5), Body.Type.STATIC);
        
        //apply images
        leftWall.setImage(new BodyImage("images/Wall.gif"));
        rightWall.setImage(new BodyImage("images/Wall.gif"));
        ceiling.setImage(new BodyImage("images/Cieling.gif"));
        
        
        
        leftWall.setPosition(new BVec2(-245, 0));
        rightWall.setPosition(new BVec2(245, 0));
        ceiling.setPosition(new BVec2(0,245));
        
        
        // make a heli
        myHeli = new Helicopter(game, ground);
        AttackManager attackM = new AttackManager(myHeli);
        myHeli.setAttackManager(attackM);
        
        //send it to game class
        game.setHeli(myHeli);
        
        
        view = game.getView();
        view.setWorld(world);
        game.setHeli(myHeli);
        
        //make the tanks
        tank1 = new Tank(game, ground, myHeli);
        tank1.move(new BVec2(-100,0));
        tank2 = new Tank(game, ground, myHeli);
        tank2.move(new BVec2(100,0));
        
        //send pointers to the attack manager
        attackM.addEnemy(tank1);
        attackM.addEnemy(tank2);
        
        //get tanks aquianted with the attack manager too
        tank1.setAttackManager(attackM);
        tank2.setAttackManager(attackM);
        
        //start simulation
        world.start();
    }

    @Override
    void destroy() {
        tank1.stopTimer();
        tank2.stopTimer();
        tank1.destroy();
        tank2.destroy();
    }
}
