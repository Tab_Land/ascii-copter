/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.*;

/**
 *
 * @author ijtaba
 * Second level
 * Aim of the game, collect 6 collectibles
 * and blow up the tank
 * to move to level 3
 */
public class Level2 extends Level {

    private Helicopter myHeli;
    //constructor initialises level
    private World world;
    private WorldView view;
    private Tank tank;
    /**
     * create level
     * @param game
     */
    public Level2(Game game){
       //make new world
       world = new World(); 
       //send it to game class
       game.setWorld(world);
        
       Body background = new Body(world);
       background.setImage(new BodyImage("images/Background2.jpeg"));
        
        // make the ground
        Body ground = new Body(world, PolygonShape.makeBox(260, 5), Body.Type.STATIC);
        ground.setPosition(new BVec2(-10, -240));
        ground.setImage(new BodyImage("images/Floor.gif"));
        // put myHeli on the ground
        
        
        
        
        //make walls
        Body leftWall = new Body(world, PolygonShape.makeBox(5, 250), Body.Type.STATIC);
        Body rightWall = new Body(world, PolygonShape.makeBox(5, 250), Body.Type.STATIC);
        //and make a cieling
        Body ceiling = new Body(world, PolygonShape.makeBox(500, 5), Body.Type.STATIC);
        
        //apply images
        leftWall.setImage(new BodyImage("images/Wall.gif"));
        rightWall.setImage(new BodyImage("images/Wall.gif"));
        ceiling.setImage(new BodyImage("images/Cieling.gif"));
        
        
        //position everything
        leftWall.setPosition(new BVec2(-245, 0));
        rightWall.setPosition(new BVec2(245, 0));
        ceiling.setPosition(new BVec2(0,245));
        
        
          Body helipad = new Body(world, PolygonShape.makeBox(50, 5), Body.Type.STATIC);
          helipad.setImage(new BodyImage("images/invisible.gif"));
      
          //heli does not get damaged by touching this
         helipad.setPosition(new BVec2(-150, -90));
        
        // make some fuel pieces
        CollectableFuel fuel1 = new CollectableFuel(game);
        CollectableFuel fuel2 = new CollectableFuel(game);
        CollectableFuel fuel3 = new CollectableFuel(game);
        CollectableFuel fuel4 = new CollectableFuel(game);
        
        //make some time pieces
        
        CollectableTime time5 = new CollectableTime(game);
        CollectableTime time6 = new CollectableTime(game);
      
        
        //place the fuel places
        fuel1.move(new BVec2(-200, 0));
        fuel2.move(new BVec2(200,0));
      
        fuel3.move(new BVec2(200,200));
        fuel4.move(new BVec2(-200,200));
        
        //and the time pieces
        time5.move(new BVec2(200,90));
        time6.move(new BVec2(-200, 90));
      
        // make a heli
        myHeli = new Helicopter(game, helipad);
        //send it to game class
        game.setHeli(myHeli);
        myHeli.putOn(helipad);
        AttackManager attackM = new AttackManager(myHeli);
        myHeli.setAttackManager(attackM);
        
        //make a tank
        tank = new Tank(game, ground, myHeli);
        tank.setAttackManager(attackM);
        attackM.addEnemy(tank);
        
        view = game.getView();
        view.setWorld(world);
        game.setHeli(myHeli);
        
        //start simulation
        world.start();
    }
    /**
     * destroy this level
     */
    @Override
    public void destroy(){
        tank.stopTimer();
        tank.destroy();
    }
}
