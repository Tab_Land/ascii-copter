/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.Body;
import city.soi.platform.BodyImage;
import city.soi.platform.CircleShape;
import city.soi.platform.Shape;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;


/**
 *This class is the parent class of everything that packs a punch in the game.
 * I've got 3 kinds of ammo: Missiles (air to target), Bombs (Air to surface) and Canonballs (surface to air)
 * I may add bullets in the future
 * This class is meant made for extension , hence abstract
 */
public abstract class Ammunition extends Body implements ActionListener{
    
    private Shooter shooter;
    /**
     * used to keep track of location variables, kind of redundant (I think it can be improved to use the Better Vector class -BVec2
     */
    public float targetx,targety,x,y;
    
    /**
     * each piece of ammo will die after a certain length of time,
     * Using swing timers to keep track of fragments/dead ammo
     * critical to free memory
     */
    public Timer timer;
    /**
     * need target for aiming and heat seeking
     */
    public Body target;
    
    
    /**
     * 
     * @param game - Game pointer
     * @param shape - shape for this ammo
     * @param shooter - Shooter who shot this
     * @param expireTime - after what length of time to destroy fragments
     * @param target - who to shoot at - aiming
     */
    public Ammunition(Game game, Shape shape, Shooter shooter, int expireTime, Body target){
        super(game.getWorld(), shape);
        this.shooter = shooter;
        this.target = target;
        getCoordinates();
        this.setBody();
        this.setGravityStrength(1);
        timer = new Timer(expireTime, this);
        timer.setActionCommand("destroy");
    }
    
    
    /**
     * Get and store coordinates of target
     */
    public void getCoordinates(){
        x = shooter.getPosition().x;
        y = shooter.getPosition().y;
        //shooterx should be renamed target x
        if(target!=null){
            targetx = target.getPosition().x;
            targety = target.getPosition().y;
        }
        else {
            targetx = x;
            targety = -250;
        }
       
    }
    
    abstract void setBody();
    abstract void setRotation();
    abstract void explode();
            
    /**
     * Shoot this piece of lead!
     * at the target of course (or not in case of bombs, they just fall down and blow up)
     *
     */
    public void shootAmmunition(){
        getCoordinates();
        BVec2 direction = new BVec2(targetx-x,targety-y);
        direction =  direction.mul(1000/direction.length());
        this.setLinearVelocity(direction);
        
        timer.start();
    }
    
    
    
    /**
     * Get thy warrior
     * @return
     */
    public Shooter getShooter(){
        return shooter;
    }
    /**
     * Timer calls this function to destroy fragments -> clear memory
     * @param acEv
     */
    @Override
    public void actionPerformed(ActionEvent acEv) {
         if("destroy".equals(acEv.getActionCommand())) destroyAmmo();
    }
     /**
      * destroy this piece of munition
      */
     public void destroyAmmo(){
         shooter.decrementShots();
         timer.stop();
         this.destroy();
    }
    /**
     * Who to shoot at. Only set enemies. Friendly fire isn't funny
     * @param target
     */
    public void setTarget(Body target){
         this.target  = target;
     }
    /**
     * You're screwed if you get a pointer back at your player
     * @return
     */
    public Body getTarget(){
         return this.target;
     }
}
