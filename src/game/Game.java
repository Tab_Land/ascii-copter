package game;

import city.soi.platform.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Main class - starts the game, maps key actions ,and controls the flow of
 * levels
 */
public class Game {

    /**
     * My player - a helicopter
     */
    private Helicopter myHeli;
    /**
     * Game over flag.
     */
    private boolean isOver, paused;
    /**
     * The World in which the game bodies move and interact.
     */
    private World world;
    /**
     * A graphical display of the world (a specialised JPanel).
     */
    private WorldView view;
    /**
     * A debug display.
     */
    private DebugViewer debugViewer;
    /**
     * Keep hold of the frame/screen
     */
    private final JFrame frame;
    private Level currentLevel;
    private int objectives, level;
    private GUI myStats;

    /**
     * Initialise a new Game.
     */
    public Game() {
        isOver = false;
        //first level ends when all collectibles are collected
        objectives = 9;
        //starting with level 1
        level = 1;
        paused = false;

        // display the view in a frame
        frame = new JFrame("Game");
        myStats = new GUI(this);

        // add some keyboard handling
        frame.addKeyListener(new java.awt.event.KeyAdapter() {
            /*
             * on key press event
             */

            @Override
            public void keyPressed(java.awt.event.KeyEvent e) {
                if (isOver) {
                    return;
                }
                int code = e.getKeyCode();
                if (code == java.awt.event.KeyEvent.VK_SPACE) {
                    //if space is pressed/ held down - fly!
                    myHeli.setFlying(true);
                } //if left arrow key is pressed/held, lean left
                else if (code == java.awt.event.KeyEvent.VK_LEFT) {
                    //only if the heli hasn't exploded
                    if (!myHeli.getExploded()) {
                        myHeli.leanLeft();
                    }
                } //if right arrow key is pressed/held, right left
                else if (code == java.awt.event.KeyEvent.VK_RIGHT) {
                    //only if the heli hasn't exploded
                    if (!myHeli.getExploded()) {
                        myHeli.leanRight();
                    }
                    // F1 key toggles display of debug view
                } else if (code == java.awt.event.KeyEvent.VK_F1) {
                    if (debugViewer == null) {
                        debugViewer = new DebugViewer(new DebugSettings(world));
                    }
                    if (debugViewer.isRunning()) {
                        debugViewer.stopViewer();
                    } else {
                        debugViewer.startViewer();

                    } //Slash key shoots seeking missiles
                } else if (code == java.awt.event.KeyEvent.VK_SLASH) {
                    myHeli.shoot("Missile");
                    //Hash key shoots air to surface bombs
                } else if (code == java.awt.event.KeyEvent.VK_NUMBER_SIGN) {
                    myHeli.shoot("Bomb");
                    //if you press key E, the heli self destructs
                } else if (code == java.awt.event.KeyEvent.VK_E) {
                    if (!myHeli.getExploded()) {
                        myHeli.explode();
                    }
                }
            }

            /**
             * Handle key release events
             */
            public void keyReleased(java.awt.event.KeyEvent e) {
                if (isOver) {
                    return;
                }
                int code = e.getKeyCode();
                if (code == java.awt.event.KeyEvent.VK_LEFT) {
                    //key has been released
                    myHeli.setArrowDown(false);
                } else if (code == java.awt.event.KeyEvent.VK_RIGHT) {
                    //key has been released
                    myHeli.setArrowDown(false);
                } else if (code == java.awt.event.KeyEvent.VK_SPACE) {
                    //stop flying, if spacebar pressed
                    myHeli.setFlying(false);

                }//cycle through enemy targets (if any) 
                else if (code == java.awt.event.KeyEvent.VK_T) {
                    myHeli.getAttackManager().nextTarget();
                } //toggle through attack/counter attack mode
                else if (code == java.awt.event.KeyEvent.VK_C) {
                    myHeli.getAttackManager().toggleCounterAttack();
                }
            }
        });

        world = new World();

        // make a view
        view = new WorldView(world, 500, 500);
        this.setView(view);

        currentLevel = new Level1(this);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(myStats, BorderLayout.NORTH);
        panel.add(view, BorderLayout.SOUTH);
        // quit the application when the game window is closed
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // display the world in the window
        frame.add(panel);

        // don't let the game window be resized
        frame.setResizable(false);
        // size the game window to fit the world view
        frame.pack();
        frame.setBackground(Color.white);
        // make the window visible
        frame.setVisible(true);

        frame.requestFocusInWindow();
    }

    /**
     * Is the game over?
     *
     * @return
     */
    public boolean isOver() {
        return isOver;
    }

    /**
     *return current level
     * @return
     */
    public int getLevel() {
        return this.level;
    }

    /**
     * End the game.
     */
    public void gameOver() {
        world.pause();
        isOver = true;
    }

    /**
     * The world in which this game is played.
     *
     * @return
     */
    public World getWorld() {
        return world;
    }

    /**
     *set world, useful when changing levels
     * @param world
     */
    public void setWorld(World world) {
        this.world = world;
    }

    /**
     * The world view.
     *
     * @return
     */
    public WorldView getView() {
        return view;
    }

    /**
     * change View reference - not really useful
     * @param view
     */
    public void setView(WorldView view) {
        this.view = view;
    }

    /**
     * get my player/helicopter
     *
     * @return
     */
    public Helicopter getHeli() {
        return myHeli;
    }

    /**
     *set my helicopter
     * @param heli
     */
    public void setHeli(Helicopter heli) {
        myHeli = heli;
    }

    /**
     * pause game
     */
    public void pauseGame() {
        world.pause();
        //timer used for stat changing and updates
        myHeli.stopTimer();
    }

    /**
     * unpause game
     */
    public void unpauseGame() {
        world.unpause();
        //timer used for stat changing and updates
        myHeli.startTimer();
    }

    /**
     * objectives help keep trigger new levels
     */
    public void objectiveCompleted() {
        //decrement objectives
        objectives--;
        //if objectes are less than 1 (not including credits level)
        if (objectives < 1 && level != 4) {
            //go to next level
            changeLevel(level + 1);
            //if an objective is completed on the credits level
        } else if (level == 4) {
            //simply cast and make new tank, because that level is never ending
            ((Credits) currentLevel).makeTank();
        }
    }

    /**
     * change level
     * @param newLevel
     */
    public void changeLevel(int newLevel) {
        //destroy current heli
        myHeli.destroy();
        //stop chopper sounds
        SoundPlayer.killChopper();
        //stop timer
        myHeli.stopTimer();
        //pause the simulation
        world.pause();
        //change level
        level = newLevel;
        //destroy current level
        currentLevel.destroy();
        //load splash loading screen
        Level oldLevel = currentLevel;
        //initiate loading screen
        currentLevel = new LevelLoading(this);
        
        //start preparing next level
        Level nextLevel = null;
        if (newLevel == 1) {
            nextLevel = new Level1(this);
            //9 collectibles needed to pass
            objectives = 9;
        } else if (newLevel == 2) {
            nextLevel = new Level2(this);
            //6 collectibles and 1 tank explosion needed 
            objectives = 7;
        } else if (newLevel == 3) {
            nextLevel = new Level3(this);
            //2 tanks
            objectives = 2;
        } else if (newLevel == 4) {
            nextLevel = new Credits(this);
            //no objectives, never ending level
        }
        //new level has now started, store a reference
        currentLevel = nextLevel;
    }

    /**
     * grab focus for mouse and keyboard events
     */
    public void resetFocus() {
        frame.requestFocusInWindow();
    }

    /**
     *restart level
     */
    public void restartLevel() {
        changeLevel(level);
        frame.requestFocusInWindow();
        updateGUI();
    }

    /**
     * initiate god mode
     */
    public void godMode() {
        //increase Time, health, and fuel greedily
        myHeli.setFuel(10000);
        myHeli.setTime(10000);
        myHeli.setHealth(10000);
        updateGUI();
        //appearance issues
        frame.pack();
    }

    /**
     * update the panel with new values
     */
    public void updateGUI() {
        myStats.setFuel(String.valueOf(myHeli.getFuel()));
        myStats.setHealth(String.valueOf(myHeli.getHealth()));
        myStats.setTime(String.valueOf(myHeli.getTime()));

    }

}
