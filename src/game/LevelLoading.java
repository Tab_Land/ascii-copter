package game;

import city.soi.platform.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *Splash level
 * Quick to load
 * keeps the user occupied however long the next level takes to laod
 * @author ijtaba
 */
public class LevelLoading extends Level{
    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    private World world;
    private WorldView view;
    
    /**
     * Create splash level
     * @param game
     */
    public LevelLoading(Game game){
       //make new world
       world = new World(); 
       //send it to game class
       game.setWorld(world);
        
       Body background = new Body(world);
       //just set a background animation, while game loads
       background.setImage(new BodyImage("images/Loading.gif"));
        // make a view
        
       view = game.getView();
       view.setWorld(world);
    }

    @Override
    void destroy() {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}


