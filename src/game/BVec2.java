package game;


import org.jbox2d.common.Vec2;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ijtaba
 * Better Vector 2 class - BVec2
 * Addressing the problems city.soi.* forgot
 * This class was really only made to address the lack of rotation functions
 * Eases vector math by reducing complexity
 * Extends Vec2 - build on top
 */
public class BVec2 extends Vec2{
    
    /**
     * Create a better vector from
     * @param x x value
     * @param y y value
     */
    public BVec2(float x, float y){
        //send to super class
        super(x,y);
    }
    /**
     * Copies a vector.
     * because casting didn't work
     * @param initial
     */
    public BVec2(Vec2 initial){
        //send to parent class
        super(initial.x,initial.y);
    }
    /**
     * Uses trigonometry rotate by an angle (about center->0,0) , counter clockwise
     * 
     * @param theta - radians, counter clockwise
     */
    public void rotate(double theta){
        //do the math bro - http://en.wikipedia.org/wiki/Trigonometry
        float cos = (float) Math.cos(theta);
        float sin = (float) Math.sin(theta);
       float nx = x * cos - y * sin; 
        float ny = x * sin + y * cos;
        
        x = nx;
        y = ny;
    }
    
    /**
     * Fancy vector math to rotate vector about another vector. Radians, counter clockwise
     * @param point - Vector position to rotate about
     * @param theta - angle to rotate, counter clockwise and in radians
     */
    public void rotateAboutPoint(Vec2 point, double theta){
        //translate point to center
        set(sub(point));
        //rotate
        rotate(theta);
        //reverse effects of translation
        set(add(point));
    }
    
    /**
     * get an angle between a vector and 0 degrees (the vector (+x,0))
     * @param point - vector to get angle for
     * @return
     */
    public double getAngleFromPoint(Vec2 point){
        double dx = point.x -x;
        double dy = point.y - y;
        //atan2 - gets the actual angle from x and y components
        double theta = Math.atan2(dy,dx);
        theta += Math.PI;
        return theta;
    }
    
    /**
     * Casting hack
     * @param a - scaler multiply by this, not local
     * @return
     */
    public BVec2 mul(float a){
        return new BVec2(super.mul(a));
    }
    
    /**
     * Casting hack Not local subtraction
     * @param b - vector to subtract
     * @return
     */
    public BVec2 sub(Vec2 b){
        return new BVec2(super.sub(b));
    }
    
}
