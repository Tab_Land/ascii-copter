package game;

import city.soi.platform.*;

/**
 * Pick-ups in a game. When the player collides with an time piece, the
 * player's time count is increased and the time piece is removed
 * from the world. 
 */
public class CollectableTime extends Body implements CollisionListener{

    /** The game in which the player is playing. */
    private Game game;

    /**
     * Initialise a new time piece.
     * @param game 
     */
    public CollectableTime(Game game) {
        super(game.getWorld(), new PolygonShape(-5.0f,8.0f, -7.0f,2.0f, -7.0f,-3.0f, -4.0f,-6.0f, 1.0f,-6.0f, 4.0f,-3.0f, 4.0f,2.0f, -1.0f,7.0f));
        this.game = game;
        this.setImage(new BodyImage("images/TimePiece.gif"));
        //must not fall
        this.setGravityStrength(0);
        //attach collision listener
        game.getWorld().addCollisionListener(this);
    }
    
    /**
     * Destroy time piece, play clock sound, and move on
     * @param collided
     */
    @Override
    public void collide(CollisionEvent collided){
           Helicopter heliref = game.getHeli();
           if(collided.getOtherBody().equals(heliref)){
               this.destroy();
               heliref.incrementTime();
               //print status to screen
               heliref.printStatus();
               //objective completed - objectives help trigger new levels
               game.objectiveCompleted();
               //play sound
               SoundPlayer.play("clockPowerUp",1f);
           }
    
    }
    
}
