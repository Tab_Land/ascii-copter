/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.*;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;

/**
 * Heavy weight tank class only real enemy
 *
 * @author ijtaba
 */
public class Tank extends Shooter implements ActionListener, CollisionListener, StepListener {

    private boolean exploded, movingLeft;
    private Game game;
    private Helicopter myHeli;
    private Timer timer;
    //firing is a timer for cannon animation
    private int health, firing;
    private Body cannon;
    private int countNextLevel;
    private AttackManager attackM;
    private ArrayList<Body> tankPieces;

    /**
     * create a tank need a heli to shoot at
     *
     * @param game - game ref
     * @param ground - the platform to be placed on
     * @param myHeli - helicopter to shoot at
     */
    public Tank(Game game, Body ground, Helicopter myHeli) {
        super(game, 10);

        game.getWorld().addStepListener(this);
        PolygonShape shape = new PolygonShape(-40.0f, -4.0f, -40.0f, -26.0f, 48.0f, -26.0f, 48.0f, -4.0f);
        PolygonShape shape2 = new PolygonShape(-6.0f, -1.0f, -5.0f, 11.0f, 16.0f, 11.0f, 16.0f, -1.0f);
        shape.setFriction(0);
        this.addShape(shape);
        this.addShape(shape2);
        this.setImage(new BodyImage("images/Tank.gif"));
        cannon = new Body(game.getWorld(), Body.Type.DYNAMIC);
        cannon.addShape(new PolygonShape(-19.5f, 5.0f, -19.5f, -4.0f, -11.5f, -4.0f, 18.5f, -1.0f, 18.5f, 2.0f, -11.5f, 5.0f));
        cannon.setImage(new BodyImage("images/Cannon.png"));
        cannon.setGravityStrength(0);
        cannon.setGhostly(true);
        this.putOn(ground);
        this.myHeli = myHeli;
        timer = new Timer(1000, this);
        timer.setActionCommand("go");
        timer.start();
        movingLeft = true;
        this.game = game;
        health = 400;
        countNextLevel = 3;
        game.getWorld().addCollisionListener(this);
    }
    /*
     * patrol left to right on the ground makes it harder for the heli to hit
     * with bombs
     */

    private void patrol() {
        if (this.getPosition().x < -100 && movingLeft) {
            moveRight();
            movingLeft = false;
        } else if (this.getPosition().x > 100 && !movingLeft) {
            moveLeft();
            movingLeft = true;
        } else if (movingLeft) {
            moveLeft();
        } else {
            moveRight();
        }

        shoot("default");

    }
    /*
     * translate towards the right
     */

    private void moveRight() {
        this.setLinearVelocity(new BVec2(60, 0));
    }
    /*
     * translate towards the left
     */

    private void moveLeft() {
        this.setLinearVelocity(new BVec2(-60, 0));
    }

    /*
     * shoot thy cannonballs
     */
    @Override
    void shootAmmo(String choice) {
        if (!exploded && !myHeli.getExploded()) {
            Cannonball ball = new Cannonball(game, this, myHeli);
            ball.setAttackManager(attackM);
            //attach cannonball to attack manager (to be shot by heli in counter attack mode)
            attackM.addAmmo(ball);
            //play cheesy sounds
            SoundPlayer.play("Cannon", 0.5f);
        }
        if (exploded && countNextLevel > 0) {
            countNextLevel--;
        } else if (exploded) {
            //game objective should be completed a few seconds after tank explosion

            timer.stop();
            //give 30 second weight to clear fragments
            timer = new Timer(30000, this);
            timer.setActionCommand("clearFragments");
            timer.start();
            game.objectiveCompleted();
        }
    }

    /**
     * timer functions land here
     * @param ae
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        //clear fragments if the command has been given, must've been 30 whole seconds
        if (ae.getActionCommand().equals("clearFragments")) {
            clearFragments();
            timer.stop();
        } else {
            //otherwise continue patrolling
            patrol();
        }
    }

    /**
     *stop timer, 
     */
    public void stopTimer() {
        timer.stop();
    }

    /**
     * set attack manager
     * @param attackM
     */
    public void setAttackManager(AttackManager attackM) {
        this.attackM = attackM;
    }

    /**
     *place cannon at the right offset from tank canopy
     */
    public void placeCannon() {
        BVec2 tank = new BVec2(getPosition());

        
        BVec2 offset = null;
        if (firing > 0) {
            offset = new BVec2(-23, 3);
            //decrement firing counter
            firing--;
        } else {
            offset = new BVec2(-29, 3);
        }
        offset.rotate(this.getAngle());
        //Center of tank
        BVec2 center = new BVec2(5, 4);
        center.set(tank.add(center));

        double targetAngle = tank.getAngleFromPoint(myHeli.getPosition());
        tank.set(tank.add(offset));
        tank.rotateAboutPoint(center, targetAngle);
        cannon.setAngle((float) targetAngle);
        cannon.setPosition(tank);
    }

    /**
     * get the coordinates of the edge where the cannonball starts its battle journey
     * @return
     */
    public BVec2 getCannonEdge() {
        BVec2 inPos = new BVec2(cannon.getPosition());
        BVec2 offset = new BVec2(-17, 0);
        offset.rotate(cannon.getAngle());
        //combine
        offset.set(offset.add(inPos));
        firing = 3;
        //rotate along tank center
        return offset;
    }

    /**
     * blow up this tank!
     */
    public void explode() {
        exploded = true;

        attackM.removeEnemy(this);

        this.destroy();
        cannon.setGhostly(false);
        cannon.setGravityStrength(1);

        //explosion soundeffect
        SoundPlayer.play("characterExplosion", 0.5f);
        PolygonShape tankBody = new PolygonShape(-44.5f, -4.0f, -40.5f, 5.0f, 39.5f, 5.0f, 43.5f, -4.0f);
        PolygonShape tankHead = new PolygonShape(-6.5f, 7.0f, -10.5f, 4.0f, -11.5f, 2.0f, -10.5f, -6.0f, 9.5f, -6.0f, 10.5f, 2.0f, 9.5f, 4.0f, 6.5f, 7.0f);
        PolygonShape x1 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape x2 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape x3 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape x4 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape x5 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape x6 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape o1 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape o2 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape o3 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape o4 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);
        PolygonShape o5 = new PolygonShape(-3.0f, 4.0f, -3.0f, -3.0f, 2.0f, -3.0f, 2.0f, 4.0f);


        //make fragments
        Body bTankBody = new Body(game.getWorld(), tankBody, Body.Type.DYNAMIC);
        Body bTankHead = new Body(game.getWorld(), tankHead, Body.Type.DYNAMIC);

        Body bX1 = new Body(game.getWorld(), x1, Body.Type.DYNAMIC);
        Body bX2 = new Body(game.getWorld(), x2, Body.Type.DYNAMIC);
        Body bX3 = new Body(game.getWorld(), x3, Body.Type.DYNAMIC);
        Body bX4 = new Body(game.getWorld(), x4, Body.Type.DYNAMIC);
        Body bX5 = new Body(game.getWorld(), x5, Body.Type.DYNAMIC);
        Body bX6 = new Body(game.getWorld(), x6, Body.Type.DYNAMIC);

        Body bO1 = new Body(game.getWorld(), o1, Body.Type.DYNAMIC);
        Body bO2 = new Body(game.getWorld(), o2, Body.Type.DYNAMIC);
        Body bO3 = new Body(game.getWorld(), o3, Body.Type.DYNAMIC);
        Body bO4 = new Body(game.getWorld(), o4, Body.Type.DYNAMIC);
        Body bO5 = new Body(game.getWorld(), o5, Body.Type.DYNAMIC);


        //set images
        bTankBody.setImage(new BodyImage("images/TankBody.gif"));
        bTankHead.setImage(new BodyImage("images/TankHead.gif"));

        bX1.setImage(new BodyImage("images/X.gif"));
        bX2.setImage(new BodyImage("images/X.gif"));
        bX3.setImage(new BodyImage("images/X.gif"));
        bX4.setImage(new BodyImage("images/X.gif"));
        bX5.setImage(new BodyImage("images/X.gif"));
        bX6.setImage(new BodyImage("images/X.gif"));

        bO1.setImage(new BodyImage("images/O.gif"));
        bO2.setImage(new BodyImage("images/O.gif"));
        bO3.setImage(new BodyImage("images/O.gif"));
        bO4.setImage(new BodyImage("images/O.gif"));
        bO5.setImage(new BodyImage("images/O.gif"));


        //position fragments
        bTankBody.setPosition(new BVec2(2, -9));
        bTankHead.setPosition(new BVec2(4, 3));

        bO1.setPosition(new BVec2(-25, -20));
        bO2.setPosition(new BVec2(-11, -20));
        bO3.setPosition(new BVec2(3, -20));
        bO4.setPosition(new BVec2(17, -20));
        bO5.setPosition(new BVec2(31, -20));

        bX1.setPosition(new BVec2(38, -20));
        bX2.setPosition(new BVec2(24, -20));
        bX3.setPosition(new BVec2(10, -20));
        bX4.setPosition(new BVec2(-4, -20));
        bX5.setPosition(new BVec2(-18, -20));
        bX6.setPosition(new BVec2(-32, -20));

        // cannon.setPosition(new BVec2(100,100));
        cannon.setRenderLayer(1);

        //push all pieces into array
        tankPieces = new ArrayList<Body>();

        tankPieces.add(bTankBody);
        tankPieces.add(bTankHead);

        tankPieces.add(bX1);
        tankPieces.add(bX2);
        tankPieces.add(bX3);
        tankPieces.add(bX4);
        tankPieces.add(bX5);
        tankPieces.add(bX6);

        tankPieces.add(bO1);
        tankPieces.add(bO2);
        tankPieces.add(bO3);
        tankPieces.add(bO4);
        tankPieces.add(bO5);
        tankPieces.add(cannon);

        for (Body piece : tankPieces) {
            //translation and rotation happens here
            
            BVec2 center = new BVec2(this.getPosition());
            float angle = this.getAngle();

            center.set(center.add(piece.getPosition()));
            piece.setAngle((float) angle);

            //rotation around center
            center.rotateAboutPoint(getPosition(), angle);
            //reposition
            piece.setPosition(center);
            //get a direction, outwards
            BVec2 direction = center.sub(getPosition());
            //normalise to a length of 600
            float scale = 600 / direction.length();
            direction.set(direction.mul(scale));
            //apply as linear velocity
            piece.setLinearVelocity(direction);
            //spin stuff around too
            piece.setAngularVelocity(this.getAngularVelocity());
        }
        //fix cannon disappearing bug..
        placeCannon();
    }

    /**
     *collision events handled here
     * @param e
     */
    @Override
    public void collide(CollisionEvent e) {
        health--;
        //if too weak, think about blowing up
        if (health < 0) {
            this.setName("destroyed");
            explode();
        }
        //if hit by cannonball, make a metalic clunk
        //I'll allow friendly fire for my enemies, as I don't like them, and I'm not nice
        if (e.getOtherBody() instanceof Cannonball) {
            SoundPlayer.play("characterHit", 0.5f);
        }
    }

    /**
     * step listener used to place the cannon
     * @param e
     */
    @Override
    public void preStep(StepEvent e) {
        placeCannon();

    }

    /**
     *
     * @param e
     */
    @Override
    public void postStep(StepEvent e) {
    }
    /*
     * get rid of all old scrap from this war zone
     */
    private void clearFragments() {
        for (Body piece : tankPieces) {
            piece.destroy();
        }
    }
}
