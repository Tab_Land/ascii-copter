/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;


/**
 *
 * @author ijtaba
 * 
 * Bomb Class. Good for beating the .. out of tanks and other ..
 * extends ammunition
 */
public class Bomb extends Ammunition implements ActionListener, CollisionListener{
    private Helicopter heliref;

    private ArrayList<Body> pieces;
    private Game game;
    private SoundClip drop;
    
    
    /**
     * Construct a bomb
     * @param game - game pointer for initialisation
     * @param heliref - player reference, cause heli drops the bomb
     */
    public Bomb(Game game, Helicopter heliref){
        //send info to super class
        super(game, new PolygonShape(-9.5f,2.5f, -14.5f,0.5f, -9.5f,-1.5f, 13.5f,-1.5f, 13.5f,2.5f), heliref, 2000, null);
        //get heli ref, cause heli drops the bomb
        this.heliref = heliref;
        
        this.game = game;
        //set Rotation, aim perpendicular to heli
        setRotation();
        //shoot!
        shootAmmunition();
        //add collision listener, must make bang and explode when hit
        game.getWorld().addCollisionListener(this);
        //bomb drop soundeffect - whistle till doomsday
        drop = SoundPlayer.play("bombDrop", 0.5f);
    }
    /*
     * destroy this piece of ammo 
     * play destruction sound
     */
    @Override
    public void destroyAmmo(){
         super.destroyAmmo();
         //stop drop whistle soundeffect
         if(drop!=null) drop.close();
         //terrify the masses with explosion sound
         SoundPlayer.play("bombExplode", 0.5f);
         //and blow up into 4 fragments
         explode();
    }
    
    /**
     * Set images, and align with shooter
     */
    @Override
    public void setBody(){
            //use this function to set bodyshape and image
      this.setImage(new BodyImage("images/Bomb.gif"));
      this.setPosition(getShooter().getPosition().add(new BVec2(0,-30)));
    }
    /**
     * Set rotation, aim perpendicular to heli
     */
    @Override
    public void setRotation(){
      BVec2 curpos = new BVec2(0,0);
      //translate to heli
      curpos.set(this.getPosition());
        //the heli angle bit
        float angle = heliref.getAngle();        
        //the perpendicular bit
        this.setAngle((float)Math.PI/2 +angle);
      
    }
    /*
     * use this for calling fragment clearing method, after a length of time (swing timer
     * helps save memory
     * and keep the war zone clean and tidy
     */
    @Override
    public void actionPerformed(ActionEvent acEv) {
          if("clearFragments".equals(acEv.getActionCommand())) clearFragments();
          //if no fragments to clean, the bomb has been falling for too long, destroy
         else destroyAmmo();
          //stop timer..
         timer.stop();
    }
    /*
     * destroy each piece of exploded bomb one by one
     * 
     */
    private void clearFragments() {
        for(Body piece : pieces){
            piece.destroy();
        }
    }
    /*
     * Shoot this piece of ammo
     */
    @Override
    public void shootAmmunition(){
        //this is the vector, pointing downwards
        BVec2 direction = new BVec2(0,-10);
        //the same vector rotated, along helicopters current axis
        direction.rotate(getShooter().getAngle());
        //normalised to a length of 1000!
        direction = direction.mul(1000/direction.length());
        //then thrust this bomb
        this.setLinearVelocity(direction);
        //start timer 
        timer.start();
    }
    /*
     * Bye bye bomby
     * the bomb has hit something, destroy!
     */
    @Override
    void explode() {
        //make 4 shapes
        PolygonShape morethan = new PolygonShape(-2.0f,2.5f, 1.0f,0.5f, -2.0f,-1.5f);
        PolygonShape morethan2 = new PolygonShape(-2.0f,2.5f, 1.0f,0.5f, -2.0f,-1.5f);
        PolygonShape equals = new PolygonShape(-2.5f,1.5f, -2.5f,-0.5f, 1.5f,-0.5f, 1.5f,1.5f);
        PolygonShape equals2 = new PolygonShape(-2.5f,1.5f, -2.5f,-0.5f, 1.5f,-0.5f, 1.5f,1.5f);
        
        //create 4 bodies
        Body bequals = new Body(game.getWorld(),equals,Body.Type.DYNAMIC);
        Body bequals2 = new Body(game.getWorld(),equals2,Body.Type.DYNAMIC);
        Body bmorethan = new Body(game.getWorld(),morethan,Body.Type.DYNAMIC);
        Body bmorethan2 = new Body(game.getWorld(),morethan2,Body.Type.DYNAMIC);
        
        //set images of those 4 bodies
        bequals.setImage(new BodyImage("images/equals.gif"));
        bequals2.setImage(new BodyImage("images/equals.gif"));
        bmorethan.setImage(new BodyImage("images/morethan.gif"));
        bmorethan.setImage(new BodyImage("images/morethan.gif"));
        
        //set positions
        bmorethan.setPosition(new BVec2(0,0));
        bequals.setPosition(new BVec2(6,0));
        bequals2.setPosition(new BVec2(12,0));
        bmorethan2.setPosition(new BVec2(18,0));
        
        //apply angle, cause more than signs should point downward
        bmorethan.setAngle((float)Math.PI);
        bmorethan2.setAngle((float)Math.PI);
        
        //create fragment array
        pieces = new ArrayList<Body>();
        pieces.add(bmorethan);
        pieces.add(bmorethan2);
        pieces.add(bequals);
        pieces.add(bequals2);
        
        //for each piece of shrapnel
        for(Body piece : pieces){
            //get current offset
            BVec2 offset = new BVec2(piece.getPosition());
            //rotate point by bomb's angle
            offset.rotateAboutPoint(new BVec2(0,0), this.getAngle());
            //add center of bomb
            offset.addLocal(this.getPosition());
            
            //each piece is offsetted
            piece.setPosition(offset);
            //rotated
            piece.setAngle(this.getAngle()+piece.getAngle());
            //and sped up, for realism (looks cool)
            piece.setAngularVelocity(this.getAngularVelocity());
            //get bomb direction
            BVec2 direction = new BVec2(getLinearVelocity());
            //transfer to new pieces
            piece.setLinearVelocity(direction);
        }
        //set timer to 3 seconds
        timer = new Timer(3000, this);
        //and this timer will sweep warzone, for shrapnel
        //the polygon budgeted is limited unfortunately. 
        timer.setActionCommand("clearFragments");
        timer.start();
    }

    /**
     * Handle bomb collision
     * @param e
     */
    @Override
    public void collide(CollisionEvent e) {
        //do not explode if hits heli, other wise blow up
         if(!e.getReceivingBody().equals(heliref)) {
                destroyAmmo();
         }
    }
    
}
