/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.*;
import java.awt.Color;

/**
 *
 * @author ijtaba
 * Final level
 * Gives credits
 * Congradulates player
 */
public class Credits extends Level {

    private Helicopter myHeli;
    //constructor initialises level
    private World world;
    private WorldView view;
    private AttackManager attackM;
    private Game game;
    private Body ground;
    
    /**
     * make new level
     * @param game - game reference
     */
    public Credits(Game game) {
        //make new world
        world = new World();
        this.game = game;
        //send it to game class
        game.setWorld(world);

        //foreground is user info material
        Body foreground = new Body(world);
        //background is just a white backdrop
        Body background = new Body(world);
        
        //set images
        foreground.setImage(new BodyImage("images/Credits.png"));
        background.setImage(new BodyImage("images/BackgroundEnd.jpeg"));
        
        //set layers, background at back, foreground at front
        background.setRenderLayer(0);
        foreground.setRenderLayer(2);
        
        // make the ground
        ground = new Body(world, PolygonShape.makeBox(260, 5), Body.Type.STATIC);
        //position it
        ground.setPosition(new BVec2(-10, -240));
        ground.setImage(new BodyImage("images/Floor.gif"));
        // put myHeli on the ground

        //create walls
        Body leftWall = new Body(world, PolygonShape.makeBox(5, 250), Body.Type.STATIC);
        Body rightWall = new Body(world, PolygonShape.makeBox(5, 250), Body.Type.STATIC);
        
        //and make a cieling
        Body ceiling = new Body(world, PolygonShape.makeBox(500, 5), Body.Type.STATIC);

        //apply images
        leftWall.setImage(new BodyImage("images/Wall.gif"));
        rightWall.setImage(new BodyImage("images/Wall.gif"));
        ceiling.setImage(new BodyImage("images/Cieling.gif"));


        //position
        leftWall.setPosition(new BVec2(-245, 0));
        rightWall.setPosition(new BVec2(245, 0));
        ceiling.setPosition(new BVec2(0, 245));


        // make a heli
        myHeli = new Helicopter(game, ground);
        //make and attach attack managers
        attackM = new AttackManager(myHeli);
        myHeli.setAttackManager(attackM);
        
        //time and fuel are not important in last level
        myHeli.stopTimer();

        //send it to game class
        game.setHeli(myHeli);

        //get view,
        view = game.getView();
        view.setWorld(world);
        game.setHeli(myHeli);
        
        //make a tank
        makeTank();
        //start simulation
        world.start();
    }
    /*
     * No unfinished business. Stub method from Level class
     */
    @Override
    void destroy() {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Make a tank, attach it to the attack manager
     */
    public void makeTank() {
        Tank tank1 = new Tank(game, ground, myHeli);
        //position
        tank1.move(new BVec2(100, 0));
        attackM.addEnemy(tank1);
        tank1.setAttackManager(attackM);
    }
}
