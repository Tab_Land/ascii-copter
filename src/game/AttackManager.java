/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import city.soi.platform.Body;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Attack manager,
 * helps coordinate attacks and counter attacks on enemy and enemy ammo bodies respectively
 * useful manager for the heat seeking missile
 * @author ijtaba
 */
public class AttackManager {

    private ArrayList<Body> enemies;
    private ArrayList<Body> enemyAmmo;
    //current attack and counter attack indexes
    //used to keep track of current targets
    private int currentAIndex;
    //true when counter attacking, false when attacking
    private boolean counterAttack;
    private Helicopter myHeli;

    /**
     * Send me thy player
     * So I shall help smite thine enemies!
     * @param heliref
     */
    public AttackManager(Helicopter heliref) {
        //initialise lists
        enemies = new ArrayList<Body>();
        enemyAmmo = new ArrayList<Body>();
        //start at first position for attack index
        currentAIndex = 0;
        //counter attack initially, toggle if needed
        counterAttack = true;
        //store a reference to the player
        myHeli = heliref;
    }

    /**
     * The current target depends on whether we are attacking or counter attacking
     * If attacking, an enemy can be chosen manually, by the "T" key (for toggle enemy)
     * If counter attacking, this class helps find the closest (hence most dangerous) piece of enemy
     * munition, and knock it out of the park
     * @return
     */
    public Body getCurrentTarget() {
        //if counter attacking
        if (counterAttack) {
            //sort enemy ammo list by distance
            distanceAmmoSort();
            if (enemyAmmo.size() > 0) {
                //if a closest piece of munition exists, find it
                return enemyAmmo.get(0);
            } else {
                //other wise, shoot into the abyss
                return null;
            }
        } else {
            //if attacking
            if (currentAIndex < enemies.size()) {
                //get currently chosen enemy
                return enemies.get(currentAIndex);
                
            } else {
                return null;
            }
        }
    }

    /**
     * Add enemy to attack list
     * @param enemy Your enemy is my enemy MUHAHAHAAH
     */
    public void addEnemy(Body enemy) {
        enemies.add(enemy);
    }

    /**
     * Remove enemy from attack list
     * @param enemy So I guess you've either killed him, or signed a treaty?
     */
    public void removeEnemy(Body enemy) {
        enemies.remove(enemy);
    }

    /**
     *  Add enemy ammo to counter attack list
     * @param ammo - Under Fire!
     */
    public void addAmmo(Body ammo) {
        enemyAmmo.add(ammo);
    }

    /**
     * Removes enemy ammo from counter attack list
     * @param ammo - Enemy Fire Calming down, I think
     */
    public void removeAmmo(Body ammo) {
        enemyAmmo.remove(ammo);
    }

    /**
     * Get my next enemy. They shall fear my wrath
     */
    public void nextTarget() {
        if (counterAttack) {
            //distance based sort, for counter attacking
            distanceAmmoSort();
        } else {
            //see if fallen off array edge
            if (currentAIndex >= enemies.size() - 1) {
                //if so start at the begining
                currentAIndex = 0;
            } else {
                //otherwise move to next enemy
                currentAIndex++;
            }
        }
    }

    /**
     * Toggle is done by C key, (counter attack toggle)
     * switches between attack and counter attack mode
     */
    public void toggleCounterAttack() {
        if (counterAttack) {
            //switch to attack
            counterAttack = false;
        } else {
            //switch to counter attack
            counterAttack = true;
        }
    }

    /**
     * Sort enemy munitions list by the special distance comparator
     */
    public void distanceAmmoSort() {
        Collections.sort(enemyAmmo, new AmmunitionComparator(myHeli));
    }
}
